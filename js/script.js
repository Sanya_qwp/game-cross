/*

  Button toggler for show/hide a block with comments by mouse click

  const commentsBtn - target for click listener
  const commentsBlock - a block that shows/hides by clicking on btn
  const activeClassName - string

  styles for _active's mod in sass folder

*/

const commentsBtn     = document.getElementById('commentsBtn')
      commentsBlock   = document.getElementById('comments')
      activeClassName = commentsBlock.className + '_active';

commentsBtn.addEventListener('click', function() {
  if (commentsBlock.classList.contains(activeClassName)) {
    commentsBlock.classList.remove(activeClassName);
  }
  else {
    commentsBlock.classList.add(activeClassName);
  }
});


/*

  Calc crossword width with panel for buttons and toggle class for wide and small screens

  const gameContainer - block for canvas and description
  const canvasWidth - width (in px) of canvas

  let spaceForGame - width (in px) for placement the canvas and panel with buttons
  let panelBlockWidth - width (in px) for panel with buttons

  styles for wide-screen and small-screen is in sass folder
  
*/

const gameContainer = document.querySelector('.game__container')
      canvasWidth = document.querySelector('.game__window').offsetWidth;

let spaceForGame = document.querySelector('.wrapper').offsetWidth
    panelBlockWidth = document.querySelector('.panel__container').offsetWidth;

function setArrangementOfElements() {
  if (spaceForGame < (canvasWidth + panelBlockWidth)) {
    gameContainer.classList.remove('wide-screen');
    gameContainer.classList.add('small-screen');
  }
  else {
    gameContainer.classList.remove('small-screen');
    gameContainer.classList.add('wide-screen');
  }
}

window.onload = function(event) {
  setArrangementOfElements();
}
window.onresize = function (event) {
  //var updates after resize
  spaceForGame = document.querySelector('.wrapper').offsetWidth;
  panelBlockWidth = document.querySelector('.panel__container').offsetWidth;

  setArrangementOfElements();
}
